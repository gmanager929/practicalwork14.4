﻿#include <iostream>
#include <string>

int main() 
{
    
    std::string myString = "Hello, World!";
    std::cout << "String: " << myString << std::endl;

    size_t length = myString.size(); 
    std::cout << "Length: " << length << std::endl;

    
    char firstChar = myString.front(); 
    std::cout << "First character: " << firstChar << std::endl;

    
    char lastChar = myString.back(); 
    std::cout << "Last character: " << lastChar << std::endl;

    return 0;
}
